from BCBio import GFF
import re


class GFFParser():

    def __init__(self, beauris_annot, re_protein="\1-P\2", re_protein_capture="^(.*?)-R([A-Z]+)$"):
        self.re_protein = re_protein
        self.re_protein_capture = re_protein_capture
        self.annotation = beauris_annot

    def _generate_protein(self, mrna_id):
        protein_id = re.sub(self.re_protein_capture, self.re_protein, mrna_id)
        return protein_id

    def parse(self):
        annot_version = self.annotation.slug()
        assembly_version = self.annotation.assembly.slug()
        genome = self.annotation.assembly.organism.pretty_name()
        gff_file = self.annotation.get_derived_path('fixed_gff')
        data = {}

        # Parse GFF, store info in dict
        for scaff in GFF.parse(gff_file):
            for topfeat in scaff.features:
                # Only check mRNA for now
                # TODO: get gene attributes also?
                if topfeat.type != 'gene':
                    continue
                for mrna in topfeat.sub_features:
                    mrna_id = mrna.qualifiers['ID']
                    gene_id = mrna.qualifiers['Parent']
                    protein_id = self._generate_prot_name(mrna_id)
                    data[protein_id] = {
                        "organism": genome,
                        "assembly": assembly_version,
                        "annotation": annot_version,
                        "gene_id": gene_id,
                        "interpro_terms": set(),
                        "diamond_terms": set(),
                        "diamond_ids": set(),
                        "interpro_ids": set(),
                        "go_ids": set(),
                        "go_terms": set(),
                        "eggnog_og_terms": set(),
                    }

        return data
