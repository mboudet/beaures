#!/usr/bin/env python
import argparse
import logging
import sys
import json
import tarfile

from beauris import Beauris

from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk

from parsers import GffParser, InterproParser, DiamondParser, EggnogParser

logging.basicConfig(level=logging.INFO)
log = logging.getLogger()


def unpack_gene(gene):
    data = {}
    for key, value in gene.items():
        data[key] = list(value) if isinstance(value, set) else value
    return data


def data_generator(data):
    for gene in data.values():
        yield {
            "_index": "genes",
            "_source": unpack_gene(gene)
        }


def send_content(es, data):
    for value in data.values():
        pass


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('infile', type=str)
    parser.add_argument('mapping', type=str)
    args = parser.parse_args()

    bo = Beauris()
    org = bo.load_organism(args.infile)

    # We need to check for both staging and production
    if 'elasticsearch' not in org.get_deploy_services("staging") and 'elasticsearch' not in org.get_deploy_services("production"):
        log.info('Elasticsearch is not required for {}'.format(org.slug()))
        sys.exit(0)

    if not org.assemblies:
        log.error('At least one assembly is required for Genoboo')
        sys.exit(0)

    task_id = "build_elasticsearch"
    runner = bo.get_runner('galaxy', org, task_id)
    config_task = runner.get_job_specs(task_id)

    re_protein = config_task.get("re_protein", "$1-P$2").replace(r"\$", "$")
    re_protein_capture = config_task.get("re_protein_capture", "^(.*?)-R([A-Z]+)$")
    es_host = config_task.get("es_host", "localhost")
    es_port = config_task.get("es_port", "80")
    es_protocol = config_task.get("es_protocol", "http")
    go_file = config_task.get("go_file")

    es_url = "{}://{}:{}".format(es_protocol, es_host, es_port)
    es = Elasticsearch(es_url)

    with open(args.mapping, 'r') as f:
        mapping = json.load(f)

    es.indices.create(index="genes", mappings=mapping)

    for ass in org.assemblies:
        for annot in ass.annotations:
            data = GffParser(annot, re_protein=re_protein, re_protein_capture=re_protein_capture).parse()
            data = InterproParser(annot, data).parse()
            data = DiamondParser(annot, data).parse()
            data = EggnogParser(annot, data, go_file).parse()

        bulk(es, data_generator(data), refresh='wait_for')

    data_path = "/usr/share/elasticsearch/data/"
    archive_path = org.get_derived_path('elasticsearch')

    with tarfile.open(archive_path, 'w:bz2') as outtarf:
        outtarf.add(data_path, arcname="es_data")
